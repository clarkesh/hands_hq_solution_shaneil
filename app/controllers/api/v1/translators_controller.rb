module Api 
  module V1 
    class TranslatorsController < ApplicationController
      def index
        @translations = Translator.all
        render json: { 
          status: 'Success', 
          message: 'Translations loaded', 
          data: @translations 
          }, status: :ok
      end

      def create
        @translation = Translator.new(input_params)
        if @translation.save
          render json: { 
            status: 'Success', 
            message: 'Translation added', 
            data: @translation
            }, status: :ok
          else
            render json: { 
              status: 'ERROR', 
              message: 'Unable to add translation', 
              data: @translation.errors
              }, status: :unprocessable_entity
        end
      end

      def destroy
        @translation = Translator.find(params[:id])
        @translation.destroy
        render json: { 
          status: 'Success', 
          message: 'Translation deleted', 
          data: @translation
          }, status: :ok
      end
      

      private 
      def input_params
        params.permit(:input, :translation)
      end
    end
  end
end
