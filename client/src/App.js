import React from 'react';
import './App.css';

import Header from './components/Header';
import Form from './components/Form';
import Translations from './components/TranslationsDisplay';

function App() {
  return (
    <div className="App">
      <Header />
      <Form />
      <Translations />
    </div>
  );
}

export default App;
