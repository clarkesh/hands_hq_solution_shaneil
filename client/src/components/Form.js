import React, { Component } from 'react';
import Axios from 'axios';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      translation: ''
    };
  }

  handleTranslation = event => {
    this.setState({
      input: event.target.value,
      translation : ''
    });
  };

  translate = text => {
    text = this.state.input;
    text = text.toLowerCase();
    
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    let vowelIndex = 0;
    if (vowels.includes(text[0])) {
      return text + 'way';
    } else {
      for (let char of text) {
        if (vowels.includes(char)) {
          vowelIndex = text.indexOf(char);
          break;
        }
      }
      const result = text.slice(vowelIndex) + text.slice(0, vowelIndex) + 'ay';
      return result
    }
  };

  handleClick =(e) =>{
    const result = this.translate(e.target.value)
    this.setState({translation: result})
  }
  handleSave = () =>{
    Axios.post('http://localhost:3001/api/v1/translators/', {
      input: this.state.input,
      translation: this.state.translation
    })
   }

  render() {
    return (
      <div>
        <label>
         Enter terminology:
          <input type="text" onChange={this.handleTranslation} />
        </label>
        <button onClick={this.handleClick}>Translate!</button>
        <button onClick={this.handleSave}>Save Translation</button>
        <hr/>
        <h2>{this.state.input}</h2>
        <p>{this.state.translation}</p>
      </div>
    );
  }
}

export default Form;
