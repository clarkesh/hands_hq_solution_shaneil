import React,{Component} from 'react';
import axios from 'axios'

class TranslationsDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      translations_list: []
    }
  }

  componentDidMount = () => {
    axios.get('http://localhost:3001/api/v1/translators/')
    .then(response => {
      this.setState({translations_list: response.data.data})
    })
    .catch(error => console.log(error))
  }

  render(){
    return(
      <div>
       {this.state.translations_list.map((list) => {
        return(
          <div key={list.id} >
            <h2>{list.input}</h2>
            <p>{list.translation}</p>
          </div>
        )       
      })}
      </div>
    )
  }
  
}

export default TranslationsDisplay;