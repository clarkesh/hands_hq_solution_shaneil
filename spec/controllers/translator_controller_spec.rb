require 'rails_helper'

RSpec.describe Api::V1::TranslatorsController, type: :controller do
  describe "GET #index" do
    before do
      get :index
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    before do
      post :create
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  # describe "DELETE #destroy" do
  #   before do
  #     delete :destroy
  #   end
  #   it "returns http success" do
  #     expect(response).to have_http_status(:success)
  #   end
  # end
end
