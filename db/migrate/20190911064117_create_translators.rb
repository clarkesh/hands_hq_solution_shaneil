class CreateTranslators < ActiveRecord::Migration[5.2]
  def change
    create_table :translators do |t|
      t.string :input
      t.text :translation

      t.timestamps
    end
  end
end
