## How to get started

### Download application files
`$ git clone https://clarkesh@bitbucket.org/clarkesh/hands_hq_solution_shaneil.git`  
`$ cd pig_latin_translator`   

### Install gem for Rails backend
`$ bundle install`   

### Create & update db
`$ rake db:create`  
`$ rake db:migrate`  

### Seed db with fake data
`$ rake db:seed`

### Start API server
`$ rails s -p 3001`  

### Start React Client
`$ cd client`  
`$ npm install`  
`$ npm start`

## Use Cases
```
As a User
so I can translate terms 
I would like to type and submit
my requests for translations on a given terminology 

As a User
so I can save terms for future 
I would like to save a term and its translation

As a User
so I can clear my list of unused terms 
I would like to delete terms and their translations I no longer need
```

## Approach
I started the task by time blocking myself to 3 hours to try and fulfill the 3 use cases above.  

1. Beginning with the backend, I created a simple API with actions to display, delete and create.  
Database was seeded with some sample data.  
2. Only 2 test specs were added to text the response of the api operations.  
3. I then went on to the front end and created this with 3 components: Header, Form and TranslationsDisplay.  
4. The header display the title, the Form supplies an area for user to input their terms for translation and TranslationsDisplay pulls saved translations from the database.  
5. I went into a few issues such as cors which ate into my time trying to figure out how to solve this.  
6. The app currently only pulls current data from the database and displays on screen. A user is able to enter a term and click translate to have the translation displayed.  
7. Currently the translation takes place on the frontend rather than the backend

## Issues
I came across a few issues that I ended up abandoning to save on time:  
- Cors errors  
- Writing specs  
- Understanding how Rails backend works with React frontend  
